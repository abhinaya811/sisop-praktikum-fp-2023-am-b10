# sisop-praktikum-fp-2023-AM-B10

Anggota :
| Nama                         | NRP        |
|------------------------------|------------|
|Raden Pandu Anggono Rasyid    | 5025201024 |
|Ryan Abinugraha               | 5025211178 |
|Abhinaya Radiansyah Listiyanto| 5025211173 |

# Membuat Program Server

membuat fungsi server dengan mengambil nama client, membaca data client, dan memprosesnya

code membuat socket
```
 if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
```

# Autentikasi
Memasukkan username dan password jika user adalah root maka dia memiliki semua hak akses database
```
if (!root) {
    char auth_response[1024] = {0};
    valread = read(new_socket, autentikasi_akun, 1024);

    int cek = cek_data(autentikasi_akun, "client_account.txt");

    int u = 0;
    for (int i = 0; i < strlen(autentikasi_akun); i++) {
        if (autentikasi_akun[i] == ',')
            break;
        username[u] = autentikasi_akun[i];
        u++;
    }

    if (cek) {
        sprintf(auth_response, "Auth berhasil\n");
    } else {
        sprintf(auth_response, "Akunmu tidak terdaftar\n");
    }
    
    send(new_socket, auth_response, sizeof(auth_response), 0);
}
```
kode menerima data autentikasi dari client menggunakan read() dan kemudian memanggil fungsi cek_data() untuk memeriksa apakah data autentikasi yang diberikan cocok dengan data yang ada dalam file "client_account.txt". Jika cocok, maka server akan mengirimkan respons "Auth berhasil" ke client, jika tidak cocok, maka respons yang dikirim adalah "Akunmu tidak terdaftar"

# Autorasi
```
if (use) {
    database_digunakan[0] = '\0';
    int val, n = 0;
    for (val = strlen(req); val > 0; val--) {
        if (req[val] == 32)
            break;
    }
    for (int i = val + 1; i < strlen(req) - 2; i++) {
        database_digunakan[n] = req[i];
        n++;
    }

    char akses[1024];
    char username[1024] = {0};
    int u = 0;
    for (int i = 0; i < strlen(autentikasi_akun); i++) {
        if (autentikasi_akun[i] == ',')
            break;
        username[u] = autentikasi_akun[i];
        u++;
    }
    sprintf(akses, "%s,%s", database_digunakan, username);
    int cek = cek_data(akses, "access_account.txt");

    if (cek || root) {
        sprintf(response, "%s USED\n", database_digunakan);
    } else {
        sprintf(response, "ACCESS DENIED!!!\n");
    }

    send(new_socket, response, strlen(response), 0);
}
```
kode perintah "USE" dari client dan mengambil nama database yang ingin digunakan. Selanjutnya, kode memeriksa apakah pengguna memiliki akses otorisasi untuk menggunakan database tersebut.

# Data Definition Language
1. Create User
    digunakan untuk dapat membuat user baru
2. Create Database
    digunakan untuk dapat membuat database baru

#  Data Manipulation Language
bagian untuk insert, update, delete, select dalam database

# client
Kode yang berfungsi untuk menyambungkan ke server yang sedang berjalan, program ini menggunakan socket untuk melakukan koneksi ke server, mengirim permintaan dari pengguna ke server, dan menerima respons dari server. Setelah koneksi terjalin, program client akan menerima masukan dari pengguna melalui fgets(), kemudian mengirimkan permintaan tersebut ke server menggunakan send(). Selanjutnya, program akan menerima respons dari server menggunakan read() dan menampilkannya ke layar.

# dump
Kode yang berfugnsi untuk backup sebuah database. Ketika program client dump dijalankan, ia akan mengirimkan permintaan "dump" ke server. Server akan merespons dengan mengirimkan semua data dari database kepada client. Client kemudian menyimpan data tersebut  sebagai file cadangan atau backup dari database.