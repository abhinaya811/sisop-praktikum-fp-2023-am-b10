#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>

#define PORT 8080

int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[1024] = {0};

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    char request[1024] = "dump";
    send(sock, request, strlen(request), 0);

    FILE *file = fopen("database_dump.txt", "w");
    if (file == NULL) {
        printf("Error opening file.\n");
        return -1;
    }

    while (1) {
        valread = read(sock, buffer, 1024);
        if (valread <= 0)
            break;
        fprintf(file, "%s", buffer);
        memset(buffer, 0, sizeof(buffer));
    }

    fclose(file);
    printf("Database dump has been saved to 'database_dump.txt'.\n");

    return 0;
}
