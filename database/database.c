#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include <time.h>
#define PORT 8000

#define DATABASE_DIR "databaseku"
#define CLIENT_ACCOUNT_FILE "client_database/client_account.txt"
#define ACCESS_ACCOUNT_FILE "client_database/access_account.txt"
#define LOG_FILE "log.txt"

int cek_data(const char data[], const char nama_file[]) {
    char path_file[1024];
    sprintf(path_file, "%s/%s", DATABASE_DIR, nama_file);

    FILE *file = fopen(path_file, "r");
    if (file == NULL) {
        perror("fopen()");
        return 0;
    }

    char line[1024];
    while (fgets(line, sizeof(line), file) != NULL) {
        if (strstr(line, data) != NULL) {
            fclose(file);
            return 1;
        }
    }

    fclose(file);
    return 0;
}

void add_log(const char log[]) {
    FILE *file = fopen(LOG_FILE, "a");
    if (file == NULL) {
        perror("fopen()");
        return;
    }

    fprintf(file, "%s\n", log);
    fflush(file);
    fclose(file);
}

int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char *hello = "Hello from server";

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    char *path = "/home/rendi/sisop";
    int root;
    char autentikasi_akun[1024] = {0};
    char username[1024] = {0};

    recv(new_socket, &root, sizeof(root), 0);
    if (!root) {
        char auth_response[1024] = {0};
        valread = read(new_socket, autentikasi_akun, sizeof(autentikasi_akun));
        int cek = cek_data(autentikasi_akun, CLIENT_ACCOUNT_FILE);

        int u = 0;
        for (int i = 0; i < strlen(autentikasi_akun); i++) {
            if (autentikasi_akun[i] == ',')
                break;
            username[u] = autentikasi_akun[i];
            u++;
        }

        if (cek) {
            sprintf(auth_response, "Auth berhasil\n");
        } else {
            sprintf(auth_response, "Akunmu tidak terdaftar\n");
        }

        send(new_socket, auth_response, sizeof(auth_response), 0);
    }
    if (root) {
        strcpy(username, "root");
    }

    char database_digunakan[1024];
    database_digunakan[0] = '\0';

    while (1) {
        time_t t = time(NULL);
        struct tm tm = *localtime(&t);
        char req[1024] = {0};
        char response[1024] = {0};
        char log[1024] = {0};

        valread = read(new_socket, req, sizeof(req));
        sprintf(log, "%d-%02d-%02d %02d:%02d:%02d:%s:%s",
                tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, username, req);
        add_log(log);
        printf("%s", req);

        char *create_user = strstr(req, "CREATE USER");
        char *create_database = strstr(req, "CREATE DATABASE");
              
        char *drop_table = strstr(req, "DROP TABLE");
        char *drop_column = strstr(req, "DROP COLUMN");
        char *use = strstr(req, "USE");

        if (create_user) {
            if (root) {
                char akun[20] = {0};
                int u = 0;
                char password[20] = {0};
                int p = 0;
                for (int i = 12; i < strlen(req); i++) {
                    if (req[i] == 32) {
                        while (1) {
                            i++;
                            if (req[i] == 32 && req[i - 1] == 'Y' && req[i - 2] == 'B') {
                                i++;
                                akun[u] = ',';
                                u++;
                                break;
                            }
                        }
                    }
                    akun[u] = req[i];
                    u++;
                }

                FILE *fakun = fopen(CLIENT_ACCOUNT_FILE, "a");
                if (fakun == NULL) {
                    perror("fopen()");
                    return EXIT_FAILURE;
                }

                fprintf(fakun, "%s\n", akun);
                fclose(fakun);
                sprintf(response, "CREATE USER SUCCESS\n");
            } else {
                sprintf(response, "ACCESS DENIED!!!\n");
            }

            send(new_socket, response, sizeof(response), 0);
        }

        if (create_database) {
            char nama_database[1024] = {0};
            int val, n = 0;
            for (val = strlen(req); val > 0; val--) {
                if (req[val] == 32)
                    break;
            }

            for (int i = val + 1; i < strlen(req) - 2; i++) {
                nama_database[n] = req[i];
                n++;
            }

            char path_database_baru[1024] = {0};
            sprintf(path_database_baru, "%s/%s", DATABASE_DIR, nama_database);

            int result = mkdir(path_database_baru, 0777);

            if (!root) {
                char access_database[1024] = {0};
                char username[1024] = {0};
                int u = 0;
                for (int i = 0; i < strlen(autentikasi_akun); i++) {
                    if (autentikasi_akun[i] == ',')
                        break;
                    username[u] = autentikasi_akun[i];
                    u++;
                }

                sprintf(access_database, "%s,%s", nama_database, username);
                FILE *faccess = fopen(ACCESS_ACCOUNT_FILE, "a");
                if (faccess == NULL) {
                    perror("fopen()");
                    return EXIT_FAILURE;
                }

                fprintf(faccess, "%s\n", access_database);
                fclose(faccess);
            }

            if (result == -1) {
                sprintf(response, "DATABASE EXIST\n");
            } else {
                sprintf(response, "DATABASE CREATED\n");
            }

            send(new_socket, response, sizeof(response), 0);
        }

        if (drop_table) {
            // Mendapatkan nama tabel yang ingin dihapus
            char table_name[1024] = {0};
            int val, n = 0;
            for (val = strlen(req); val > 0; val--) {
                if (req[val] == 32)
                    break;
            }
            for (int i = val + 1; i < strlen(req) - 2; i++) {
                table_name[n] = req[i];
                n++;
            }

            // Melakukan pengecekan otorisasi dan menghapus tabel jika diperbolehkan
            if (root || (database_digunakan[0] != '\0' && check_table_access(database_digunakan, username, table_name))) {
                char path_table[1024];
                sprintf(path_table, "databases/%s/%s", database_digunakan, table_name);

                if (remove_file(path_table) == 0) {
                    sprintf(response, "DROP TABLE SUCCESS\n");
                } else {
                    sprintf(response, "DROP TABLE FAILED\n");
                }
            } else {
                sprintf(response, "ACCESS DENIED!!!\n");
            }

            send(new_socket, response, strlen(response), 0);
        }

        if (drop_column) {
            // Mendapatkan nama kolom yang ingin dihapus
            char column_name[1024] = {0};
            int val, n = 0;
            for (val = strlen(req); val > 0; val--) {
                if (req[val] == 32)
                    break;
            }
            for (int i = val + 1; i < strlen(req) - 2; i++) {
                column_name[n] = req[i];
                n++;
            }

            // Melakukan pengecekan otorisasi dan menghapus kolom jika diperbolehkan
            if (root || (database_digunakan[0] != '\0' && check_table_access(database_digunakan, username, table_name))) {
                char path_table[1024];
                sprintf(path_table, "databases/%s/%s", database_digunakan, table_name);

                if (remove_column_from_table(path_table, column_name) == 0) {
                    sprintf(response, "DROP COLUMN SUCCESS\n");
                } else {
                    sprintf(response, "DROP COLUMN FAILED\n");
                }
            } else {
                sprintf(response, "ACCESS DENIED!!!\n");
            }

            send(new_socket, response, strlen(response), 0);
        }

        if (use) {
            char nama_database[1024] = {0};
            int val, n = 0;
            for (val = strlen(req); val > 0; val--) {
                if (req[val] == 32)
                    break;
            }

            for (int i = val + 1; i < strlen(req) - 2; i++) {
                nama_database[n] = req[i];
                n++;
            }

            char path_database[1024] = {0};
            sprintf(path_database, "%s/%s", DATABASE_DIR, nama_database);

            if (access(path_database, F_OK) == -1) {
                sprintf(response, "DATABASE NOT FOUND\n");
            } else {
                strcpy(database_digunakan, nama_database);
                sprintf(response, "DATABASE CHANGE TO %s\n", database_digunakan);
            }

            send(new_socket, response, sizeof(response), 0);
        }

        if (strstr(req, "SHOW DATABASES")) {
            DIR *dir;
            struct dirent *ent;
            if ((dir = opendir(DATABASE_DIR)) != NULL) {
                while ((ent = readdir(dir)) != NULL) {
                    if (strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0) {
                        strcat(response, ent->d_name);
                        strcat(response, "\n");
                    }
                }
                closedir(dir);
            } else {
                perror("opendir()");
                return EXIT_FAILURE;
            }

            send(new_socket, response, sizeof(response), 0);
        }

        if (strstr(req, "SHOW TABLES")) {
            if (database_digunakan[0] == '\0') {
                sprintf(response, "Tidak ada database yang digunakan\n");
                send(new_socket, response, sizeof(response), 0);
                continue;
            }

            char path_database[1024] = {0};
            sprintf(path_database, "%s/%s", DATABASE_DIR, database_digunakan);

            DIR *dir;
            struct dirent *ent;
            if ((dir = opendir(path_database)) != NULL) {
                while ((ent = readdir(dir)) != NULL) {
                    if (strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0) {
                        strcat(response, ent->d_name);
                        strcat(response, "\n");
                    }
                }
                closedir(dir);
            } else {
                perror("opendir()");
                return EXIT_FAILURE;
            }

            send(new_socket, response, sizeof(response), 0);
        }

        if (strstr(req, "DROP USER")) {
            if (!root) {
                sprintf(response, "ACCESS DENIED!!!\n");
                send(new_socket, response, sizeof(response), 0);
                continue;
            }

            char username[1024] = {0};
            int u = 0;
            for (int i = 10; i < strlen(req); i++) {
                if (req[i] == 32)
                    break;
                username[u] = req[i];
                u++;
            }

            FILE *file = fopen(CLIENT_ACCOUNT_FILE, "r");
            if (file == NULL) {
                perror("fopen()");
                return EXIT_FAILURE;
            }

            FILE *temp = fopen("temp.txt", "w");
            if (temp == NULL) {
                perror("fopen()");
                return EXIT_FAILURE;
            }

            char line[1024];
            int found = 0;
            while (fgets(line, sizeof(line), file) != NULL) {
                if (strstr(line, username) == NULL) {
                    fprintf(temp, "%s", line);
                } else {
                    found = 1;
                }
            }

            fclose(file);
            fclose(temp);

            if (found) {
                remove(CLIENT_ACCOUNT_FILE);
                rename("temp.txt", CLIENT_ACCOUNT_FILE);
                sprintf(response, "DROP USER SUCCESS\n");
            } else {
                remove("temp.txt");
                sprintf(response, "DROP USER FAILED\n");
            }

            send(new_socket, response, sizeof(response), 0);
        }

        if (strstr(req, "DROP DATABASE")) {
            char nama_database[1024] = {0};
            int val, n = 0;
            for (val = strlen(req); val > 0; val--) {
                if (req[val] == 32)
                    break;
            }

            for (int i = val + 1; i < strlen(req) - 2; i++) {
                nama_database[n] = req[i];
                n++;
            }

            if (!root) {
                char access_database[1024] = {0};
                char username[1024] = {0};
                int u = 0;
                for (int i = 0; i < strlen(autentikasi_akun); i++) {
                    if (autentikasi_akun[i] == ',')
                        break;
                    username[u] = autentikasi_akun[i];
                    u++;
                }

                sprintf(access_database, "%s,%s", nama_database, username);
                FILE *file = fopen(ACCESS_ACCOUNT_FILE, "r");
                if (file == NULL) {
                    perror("fopen()");
                    return EXIT_FAILURE;
                }

                FILE *temp = fopen("temp.txt", "w");
                if (temp == NULL) {
                    perror("fopen()");
                    return EXIT_FAILURE;
                }

                char line[1024];
                while (fgets(line, sizeof(line), file) != NULL) {
                    if (strstr(line, access_database) == NULL) {
                        fprintf(temp, "%s", line);
                    }
                }

                fclose(file);
                fclose(temp);

                remove(ACCESS_ACCOUNT_FILE);
                rename("temp.txt", ACCESS_ACCOUNT_FILE);
            }

            char path_database[1024] = {0};
            sprintf(path_database, "%s/%s", DATABASE_DIR, nama_database);

            if (access(path_database, F_OK) == -1) {
                sprintf(response, "DATABASE NOT FOUND\n");
            } else {
                remove(path_database);
                sprintf(response, "DATABASE DELETED\n");
            }

            send(new_socket, response, sizeof(response), 0);
        }

        if (strstr(req, "EXIT")) {
            sprintf(response, "Goodbye!\n");
            send(new_socket, response, sizeof(response), 0);
            break;
        }
    }
    return 0;
}
